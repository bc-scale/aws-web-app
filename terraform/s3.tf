resource "aws_s3_bucket" "s3_logs_bucket" {
  bucket = "s3-log-bucket-odagen"
  region = var.aws_region
  force_destroy = "true"
}