resource "aws_security_group" "lb-sg-http" {
  name_prefix        = "app-sg-"
  vpc_id      = aws_vpc.app-vpc.id

  ingress {
    from_port   = var.app_alb_port_http
    to_port     = var.app_alb_port_http
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "http_lb_allow_all"
  }
}

resource "aws_security_group" "instance-sg-http" {
  name_prefix        = "instance-sg-"
  vpc_id      = aws_vpc.app-vpc.id

  ingress {
    from_port   = var.app_instance_port
    to_port     = var.app_instance_port
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "http_allow_all"
  }
}

resource "aws_security_group" "instance-sg-ssh" {
  name_prefix        = "instance-sg-"
  vpc_id      = aws_vpc.app-vpc.id

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ssh_allow_all"
  }
}

resource "aws_security_group" "instance-sg-journal-gatewayd" {
  name_prefix        = "instance-sg-"
  vpc_id      = aws_vpc.app-vpc.id

  ingress {
    from_port   = "19531"
    to_port     = "19531"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "journal_allow_all"
  }
}

resource "aws_security_group" "instance-sg-ntp" {
  name_prefix        = "instance-sg-"
  vpc_id      = aws_vpc.app-vpc.id

  egress {
    from_port   = "123"
    to_port     = "123"
    protocol    = "UDP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ntp"
  }
}